# Поиск уникальных строк во входных данных

#### Запуск
```
docker compose up -d postgres
docker compose exec -it postgres psql -U postgres -f /sql/schema.sql
docker compose exec -it postgres psql -U postgres -f /sql/data.sql
docker compose up --build app
```