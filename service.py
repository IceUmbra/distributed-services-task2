import psycopg

query = """
SELECT last_name, first_name, birth_date
FROM students
ORDER BY birth_date;
"""

try:
    with psycopg.connect("postgresql://postgres:postgres@postgres:5432/postgres") as conn:
        with conn.cursor() as cur:
            cur.execute(query)
            results = cur.fetchall()
        
        for row in results:
            print(row)

except psycopg.OperationalError as e:
    print(f"При работе с базой произошла ошибка {e}")