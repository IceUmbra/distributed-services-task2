-- Создание таблицы студентов
CREATE TABLE students (
    student_id INT PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    birth_date DATE NOT NULL
);

-- Создание таблицы предметов
CREATE TABLE subjects (
    subject_id INT PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

-- Создание таблицы экзаменов
CREATE TABLE exams (
    exam_id INT PRIMARY KEY,
    student_id INT,
    subject_id INT,
    exam_date DATE NOT NULL,
    teacher_name VARCHAR(150) NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id)
);